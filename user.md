# User

## Introdução

Recurso criado a fim de permitir a criação de um novo usuário que deverá ser informado no momento da criação de um novo post/thread.

## Serviço

* [POST /api/user/create]

##### Exemplo de Requisição

* **POST /api/user/create**

```json
{
 "email" : "eduk@teste.com",
 "username" : "Eduk",
 "password" : "password"
}
```

##### Exemplo de Resposta

 * **Code:** 200 
   **Content:** `{ 1 }`

**Note:** O id do usuário retornado como no exemplo acima, deve ser usado no atributo "userId" no momento da criação de um post/thread.