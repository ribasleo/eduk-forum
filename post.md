# Post

## Introdução

Recurso criado a fim de permitir a criação de um novo post/thread e também comentar posts/threads existentes.

**Note:** É necessário informar um usuário válido (userId) para a criação de um post/thread.

## Serviços

* [GET /api/post/list]
* [POST /api/post/create]
* [POST /api/post/comment]

##### Exemplo de Requisição

* **GET /api/post/list**

##### Exemplo de Resposta

 * **Code:** 200

```json
[
  {
    "id": 1,
    "rootId": null,
    "parentId": null,
    "user": {
      "id": 1,
      "email": "eduk@teste.com",
      "name": "teste",
      "password": "$2a$12$CRGxyNp3/aLTcsj9qI4.n.VNIdo8PJP5uHJPzwb91Pv7S4rma1.RO"
    },
    "title": "Teste Title",
    "body": "Teste Message",
    "comments": []
  }
]
```
##### Exemplo de Requisição

* **POST /api/post/create**

```json
{
 "body" : "Teste Message",
 "title" : "Teste Title", 
 "userId": 1
}
```
##### Exemplo de Resposta

 * **Code:** 200

##### Exemplo de Requisição

* **POST /api/post/comment**

```json
{ 
 "body":"Teste Message", 
 "userId": 1, 
 "rootId": 1, 
 "parentId": 1, 
 "title": "Teste Title" 
}
```
##### Exemplo de Resposta

 * **Code:** 200 



