# Banco de Dados

## Guia de Configuração Passo-a-Passo
1. Crie uma nova conexão com o banco de dados MySQL com os seguintes dados:
* usuário: eduk
* senha: edukpass01 
2. Após o passo anterior, navegue até a pasta: eduk-forum/src/main/resources/database e copie o conteúdo ou importe o initialization-script.sql e execute-o.
3. Caso não ocorra nenhum problema com os passos anteriores, o banco de dados estará apto a ser utilizado.
