package eduk.forum.email;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import eduk.forum.dao.PostDAO;
import eduk.forum.domain.Post;

public class EmailOperation {

	static final Logger logger = Logger.getLogger(EmailOperation.class);

	@Autowired
	private PostDAO postDao;

	public void sendEmail(final String commentTitle, final List<Long> userIds){
		Properties emailProperties = loadEmailProperties();
		List<String> emailAdresses = getEmailAdresses(userIds);
		try {  
			logger.info("Starting to send a email");  
			for(String email: emailAdresses){
				Session session = getSession(emailProperties); 
				Message message = createEmailMessage(commentTitle, session, emailProperties, email); 
				Transport.send(message);
				logger.info("Email was sent successfully");  
			}
		} catch (MessagingException e) {
			logger.error(e);
		}  
	} 

	private List<String> getEmailAdresses(final List<Long> postIds){
		List<String> emails = new ArrayList<String>();
		for(final Long id: postIds){
			Post post = postDao.getPost(id);
			post.getTitle();
			emails.add(post.getUser().getEmail());
		}
		return emails;
	}

	private Properties loadEmailProperties(){
		InputStream inputFile = getClass().getClassLoader().getResourceAsStream("javamail.properties");
		Properties properties = new Properties();
		try {
			logger.info("Loading javamail properties");  
			properties.load(inputFile);
			logger.info("Javamail properties was loaded successfully");  
		} catch (IOException e) {
			logger.error(e);
		}
		return properties;
	}

	private Session getSession(final Properties emailProperties){
		Session session = Session.getDefaultInstance(emailProperties, 
				new javax.mail.Authenticator() { 
			protected PasswordAuthentication getPasswordAuthentication() { 
				return new PasswordAuthentication(emailProperties.getProperty("mail.smtp.user"),emailProperties.getProperty("mail.smtp.password")); 
			} 
		}); 
		return session;
	}

	private Message createEmailMessage(final String commentTitle, final Session session, final Properties emailProperties, final String receiverEmailAddress){
		Message message = new MimeMessage(session);
		try {
			logger.info("Starting to create a email message");  
			message.setFrom(new InternetAddress(emailProperties.getProperty("mail.smtp.user")));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(receiverEmailAddress));  
			message.setSubject("Eduk Forum");  
			message.setText(commentTitle);  
			logger.info("Message was created successfully");  
		} catch (MessagingException e) {
			logger.error(e);
		}
		return message;
	}

}
