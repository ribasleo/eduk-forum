package eduk.forum.blacklist;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BlacklistOperation {

	public String checkWords(final String post){
		List<String> postWords = Arrays.asList(post.split(" "));
		List<String> badWords = loadBlackList();
		List<String> newWords = new ArrayList<String>();

		for(String postWord: postWords){
			boolean prohibitedWord = false;
			for(String badWord: badWords){
				if(postWord.toLowerCase().equals(badWord)){
					prohibitedWord = true;
					newWords.add(applyCensorship(postWord));
					break;
				}
			}
			if(prohibitedWord != true){
				newWords.add(postWord);
			}
		}
		String filteredPost = String.join(" ", newWords);
		return filteredPost;
	}

	private List<String> loadBlackList(){
		InputStream inputFile = getClass().getClassLoader().getResourceAsStream("blacklist.txt");
		List<String> badWords = new ArrayList<String>();

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(inputFile);      
		scanner.useDelimiter(";");

		while(scanner.hasNext()){
			String word = scanner.next();
			badWords.add(word);
		}
		return badWords;
	}

	private String applyCensorship(final String word){
		String censoredWord = word.replaceAll("[A-Za-z]", "*");
		return censoredWord;
	}
	
}
