package eduk.forum.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eduk.forum.dao.UserDAO;
import eduk.forum.domain.User;

public class UserDAOImpl implements UserDAO {
	
	static final Logger logger = Logger.getLogger(UserDAOImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	Session session = null;
		
	@Override
	public Long createUser(final User newUser) {
		Long userId = null;
		session = sessionFactory.openSession();
		userId = (Long) session.save(newUser);
		session.close();
		return userId;
	}

	@Override
	public User getById(final Long id) {
		User user = null;
		session = sessionFactory.openSession();
		user = (User) session.get(User.class, id);
		session.close();
		return user;
	}

}
