package eduk.forum.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eduk.forum.blacklist.BlacklistOperation;
import eduk.forum.dao.PostDAO;
import eduk.forum.domain.Post;
import eduk.forum.dto.PostDTO;

public class PostDAOImpl implements PostDAO{
	
	static final Logger logger = Logger.getLogger(PostDAOImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	private BlacklistOperation blackListOperation;
	
	Session session = null;
	
	@Override
	public List<PostDTO> getPosts() {
		session = sessionFactory.openSession();
		String hql = "from Post p left join fetch p.comments where p.rootId is null";
		Query query = session.createQuery(hql).setMaxResults(10);
		@SuppressWarnings("unchecked")
		List<PostDTO> postsDto = query.list();
		session.close();
		return postsDto;
	}

	@Override
	public void createPost(final Post post) {
		Post postChecked = filterMessage(post);
		session = sessionFactory.openSession();
		session.save(postChecked);
		session.close();
	}

	@Override
	public Post getPost(final Long id) {
		Post post = null;
		try {
			logger.info("Session load operation started");
			session = sessionFactory.openSession();
			post = (Post) session.load(Post.class, id);
			session.close();
			logger.info("Session load operation finished...");
		}catch(Exception exe){
			logger.error(exe.getStackTrace());
		}
		return post;
	}
	

	private Post filterMessage(final Post comment){
		Post postToCheck = comment;
		String titleChecked = blackListOperation.checkWords(postToCheck.getTitle());
		String bodyChecked = blackListOperation.checkWords(postToCheck.getBody());
		postToCheck.setTitle(titleChecked);
		postToCheck.setBody(bodyChecked);
		return postToCheck;
	}

	@Override
	public Post getParentPost(final Long parentId) {
		Post post = null;
		try {
			String hql = "from Post p where p.parentId = :parentId";
			Query query = session.createQuery(hql).setParameter("parentId", parentId);
			session = sessionFactory.openSession();
			post = (Post) query.uniqueResult();
			session.close();
		}catch(Exception exe){
			logger.error(exe.getStackTrace());
		}
		return post;
	}
}
