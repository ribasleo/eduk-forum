package eduk.forum.dao;

import eduk.forum.domain.User;

public interface UserDAO {

	public Long createUser(User newUser);
	
	public User getById(Long id);
	
}
