package eduk.forum.dao;

import java.util.List;

import eduk.forum.domain.Post;
import eduk.forum.dto.PostDTO;

public interface PostDAO {

	List<PostDTO> getPosts();
	
	public void createPost(Post post);

	Post getPost(Long id);
	
	Post getParentPost(Long id);
	
}
