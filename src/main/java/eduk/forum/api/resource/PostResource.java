package eduk.forum.api.resource;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import eduk.forum.api.service.PostService;
import eduk.forum.dto.NewPostDTO;
import eduk.forum.dto.PostDTO;

@Controller
@RequestMapping("/post")
public class PostResource{
	
	static final Logger logger = Logger.getLogger(PostResource.class);
	
	@Autowired
	private PostService postService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody List<PostDTO> list() {
		List<PostDTO> postList = null;
		try {
			logger.info("Initializing the listing of posts");
			postList = postService.getPosts();
			logger.info("The listing of a posts has finished");
		} catch (final Exception e) {
			logger.error(e.getMessage());
		}
		return postList;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void create(@RequestBody final NewPostDTO newPostDTO) {
		try {
			logger.info("Initializing the creation of a post");
			postService.createPost(newPostDTO);
			logger.info("The creation of a post has finished");
		} catch (final Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/comment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void comment( @RequestBody final NewPostDTO newComment) {
		try {
			logger.info("Initializing the creation of a comment to a Post");
			postService.addComment(newComment);
			logger.info("The comment to a Post has finished");
		} catch (final Exception e) {
			logger.error(e.getMessage());
		}
	}
}
