package eduk.forum.api.resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import eduk.forum.api.service.UserService;
import eduk.forum.dto.NewUserDTO;

@Controller
@RequestMapping("/user")
public class UserResource {
	
	static final Logger logger = Logger.getLogger(UserResource.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = "application/json")
	public @ResponseBody Long create(@RequestBody final NewUserDTO newUserDTO) {
		Long userId = null;
		try {
			logger.info("Initializing the creation of a user");
			logger.info(newUserDTO);
			userId = userService.createUser(newUserDTO);
			logger.info("The creation of a user has finished");
		} catch (final Exception e) {
			logger.error(e.getMessage());
		}
		return userId;
	}
}
