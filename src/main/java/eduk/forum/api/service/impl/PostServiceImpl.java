package eduk.forum.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import eduk.forum.api.service.PostService;
import eduk.forum.api.service.UserService;
import eduk.forum.blacklist.BlacklistOperation;
import eduk.forum.dao.PostDAO;
import eduk.forum.domain.Post;
import eduk.forum.domain.User;
import eduk.forum.dto.NewPostDTO;
import eduk.forum.dto.PostDTO;
import eduk.forum.email.EmailOperation;

public class PostServiceImpl implements PostService {
	
	static final Logger logger = Logger.getLogger(PostServiceImpl.class);

	@Autowired
	private PostDAO postDao;

	@Autowired
	private UserService userService;

	@Autowired
	private EmailOperation emailOperation;
	
	@Autowired
	private BlacklistOperation blacklistOperation;

	@Override
	public List<PostDTO> getPosts() {
		return postDao.getPosts();
	}

	@Override
	public Post getPost(Long id) {
		logger.info("Initializing the search for a Post by id");
		Post post = postDao.getPost(id);
		logger.info("The search for a post by id has finished");
		return post;
	}

	@Override
	public void createPost(final NewPostDTO newPostDTO) {
		logger.info("Initializing the creation of a Post");
		postDao.createPost(dtoToDomain(newPostDTO));
		logger.info("The creation of a post has finished");
	}

	private Post dtoToDomain(final NewPostDTO newPostDTO){
		Post post = new Post();
		post.setUser(userService.getById(newPostDTO.getUserId()));
		post.setTitle(newPostDTO.getTitle());
		post.setBody(newPostDTO.getBody());
		post.setRootId(null);
		return post;
	}

	@Override
	public void addComment(final NewPostDTO newComment) {
		logger.info("Initializing the comment to a Post");
		Post post = new Post();
		User user = userService.getById(newComment.getUserId());
		String commentTitle = blacklistOperation.checkWords(newComment.getTitle());
		post.setUser(user);
		post.setTitle(commentTitle);
		post.setBody(newComment.getBody());
		post.setParentId(newComment.getParentId());
		post.setRootId(newComment.getRootId());
		postDao.createPost(post);
		emailOperation.sendEmail(commentTitle, getParentIds(newComment.getParentId()));
		logger.info("The comment to a Post has finished");
	}

	private List<Long> getParentIds(final Long parentId){
		final List<Long> ids = new ArrayList<Long>();
		Post post = postDao.getPost(parentId);
		while(post.getRootId() != null){
			ids.add(post.getParentId());
			post = postDao.getPost(post.getParentId());
		}
		ids.add(post.getId());
		return ids;
	}
}
