package eduk.forum.api.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import eduk.forum.api.service.UserService;
import eduk.forum.dao.UserDAO;
import eduk.forum.domain.User;
import eduk.forum.dto.NewUserDTO;
import eduk.forum.security.BcryptUtility;

public class UserServiceImpl implements UserService {

	static final Logger logger = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDAO userDao;
	
	@Autowired
	private BcryptUtility BCrypt;
    
	@Override
    @Transactional
	public Long createUser(final NewUserDTO newUserDTO) {
		Long userId = null;
		logger.info("Initializing the creation of a user");
		userId = userDao.createUser(dtoToDomain(newUserDTO));
		logger.info("The creation of a user has finished");
		return userId;
	}
	
	private User dtoToDomain(final NewUserDTO newUserDTO){
		User user = new User();
		user.setEmail(newUserDTO.getEmail());
		user.setName(newUserDTO.getUsername());
		user.setPassword(BCrypt.encryptPassword(newUserDTO.getPassword()));
		return user;
	}

	@Override
	public User getById(final Long id) {
		return userDao.getById(id);
	}

}
