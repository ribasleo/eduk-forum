package eduk.forum.api.service;

import eduk.forum.domain.User;
import eduk.forum.dto.NewUserDTO;

public interface UserService {

	public Long createUser(NewUserDTO newUserDTO);
	
	public User getById(Long long1);
	
}
