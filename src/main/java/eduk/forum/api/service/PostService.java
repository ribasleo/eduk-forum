package eduk.forum.api.service;

import java.util.List;

import eduk.forum.domain.Post;
import eduk.forum.dto.NewPostDTO;
import eduk.forum.dto.PostDTO;


public interface PostService {
	
	public Post getPost( Long id );
	
	public List<PostDTO> getPosts();	
	
	public void createPost( NewPostDTO newPostDTO );
	
	public void addComment(NewPostDTO newComment);

}
