package eduk.forum.dto;

import eduk.forum.domain.Post;


public class PostDTO {
	
	private Long id;
    private String title;
    private String body;
    private String emailUser;
    
    public PostDTO(Post p) {
    	this.id = p.getId();
    	this.title = p.getTitle();
    	this.body = p.getBody();
        }
    
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getBody() {
	return body;
    }

    public void setBody(String body) {
	this.body = body;
    }

	public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}    
}
