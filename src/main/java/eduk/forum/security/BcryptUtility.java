package eduk.forum.security;

import org.mindrot.jbcrypt.BCrypt;

public class BcryptUtility {
	
	public String encryptPassword(final String password){
		String originalPassword = password;
        String generatedSecuredPassword = BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
		return generatedSecuredPassword;
	}
}
