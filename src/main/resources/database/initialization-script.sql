CREATE DATABASE forum_eduk;

use forum_eduk;

create table user (
  id bigint unsigned not null auto_increment,
  email varchar(300) not null,
  user_name varchar(100) not null,
  user_password varchar(100) not null,
  constraint pk_user primary key (id)  
) engine=innodb default charset=utf8; 

create table post (
  id bigint unsigned not null auto_increment,
  root_id bigint unsigned,
  parent_id bigint unsigned,
  user_id bigint unsigned not null,
  title varchar(500) not null,
  body text not null,
  constraint pk_post primary key (id),
  constraint fk_p_user_id foreign key (user_id) references user (id)
) engine=innodb default charset=utf8;
    
    
    
    
    
    
    
    
    
    
    
    