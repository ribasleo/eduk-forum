package eduk.forum.post;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import eduk.forum.api.service.PostService;
import eduk.forum.config.TestPersistenceConfig;
import eduk.forum.domain.Post;
import eduk.forum.domain.User;
import eduk.forum.dto.NewPostDTO;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager="txManager")
@ContextConfiguration(locations={"file:src/main/resources/spring-config.xml"})
public class PostResourceTest extends TestPersistenceConfig{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	private PostService postService;
	
	Session session = null;
	
	@Before
	public void setUp() {
		super.executeScript();
		session = sessionFactory.openSession();
	}

	@After
	public void tearDown(){
		session.close();
	}
	
	@AfterClass
	public static void cleanAll(){
		TestPersistenceConfig.cleanDatabase();
	}

	@Test
	@Transactional
	public void testPost(){
		User user = createUser("Eduk", "password", "eduk@teste.com");
		
		createPost("test", "test", user, new Long(1), new Long(1));
		
		String hql = "FROM Post p";
		Query query = session.createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Post> posts = query.list();
		assertEquals(1, posts.size());
	}
	
	@Test
	@Transactional
	public void testRootPost(){
		User user = createUser("Eduk", "password", "eduk@teste.com");
		
		createRootPost("test", "test", user);
		
		String hql = "FROM Post p";
		Query query = session.createQuery(hql);
		
		Post post = (Post) query.uniqueResult();
		assertEquals(null, post.getRootId());
		assertEquals(null, post.getParentId());
	}
	
	@Test
	@Transactional
	public void testCommentToPost(){
		User user = createUser("Eduk", "password", "eduk@teste.com");

		createRootPost("testRootPost", "testRootPost", user);
		createCommentToPost("testCommentPost", "testCommentPost", user, new Long(1), new Long(1));
		
		String hql = "FROM Post p where p.rootId = :rootId";
		Query query = session.createQuery(hql).setParameter("rootId", new Long(1));
		
		Post post = (Post) query.uniqueResult();
		
		assertEquals(1, post.getRootId().intValue());
	}
	
	@Test
	@Transactional
	public void testCommentToComment(){
		User user = createUser("Eduk", "password", "eduk@teste.com");

		createRootPost("testRootPost", "testRootPost", user);
		createCommentToPost("testCommentPost", "testCommentPost", user, new Long(1), new Long(1));
		
		createCommentToComment("testCommentToCommentPost", "testCommentToCommentPost", user, new Long(1), new Long(2));
		
		String hql = "FROM Post p where p.rootId = :rootId";
		Query query = session.createQuery(hql).setParameter("rootId", new Long(1));
		
		@SuppressWarnings("unchecked")
		List<Post> posts = query.list();
		
		assertEquals(2, posts.size());
	}
	
	@Test
	@Transactional
	public void testCensoredPost(){
		User user = createUser("Eduk", "password", "eduk@teste.com");

		NewPostDTO newPost = createPostDTO("Teste pt Teste", "pSdB pMdb PSOL", user);
		postService.createPost(newPost);
		
		String hql = "FROM Post p where p.rootId  is null";
		Query query = session.createQuery(hql);
		
		String title = "Teste ** Teste";
		String body = "**** **** ****";
		
		Post post = (Post) query.uniqueResult();
		
		assertEquals(title, post.getTitle());
		assertEquals(body, post.getBody());
	}
	
	private NewPostDTO createPostDTO(String title, String body, User user) {
		NewPostDTO postDto = new NewPostDTO();
		postDto.setRootId(null);
		postDto.setParentId(null);
		postDto.setBody(body);
		postDto.setTitle(title);
		postDto.setUserId(user.getId());
		return postDto;
	}
	
	private void createPost(String title, String body, User user, Long rootId, Long parentId) {
		Post post = new Post();
		post.setTitle(title);
		post.setBody(body);
		post.setUser(user);
		post.setRootId(rootId);
		post.setParentId(parentId);
		session.save(post);
	}
	
	private void createRootPost(String title, String body, User user) {
		Post post = new Post();
		post.setTitle(title);
		post.setBody(body);
		post.setUser(user);
		session.save(post);
	}
	
	private void createCommentToPost(String title, String body, User user, Long rootId, Long parentId) {
		Post post = new Post();
		post.setTitle(title);
		post.setBody(body);
		post.setUser(user);
		post.setRootId(rootId);
		post.setParentId(parentId);
		session.save(post);
	}

	private void createCommentToComment(String title, String body, User user, Long rootId, Long parentId) {
		Post post = new Post();
		post.setTitle(title);
		post.setBody(body);
		post.setUser(user);
		post.setRootId(rootId);
		post.setParentId(parentId);
		session.save(post);
	} 

	private User createUser(String username, String password, String email) {
		User user = new User();
		user.setName(username);
		user.setPassword(password);
		user.setEmail(email);
		session.save(user);
		return user;
	}

}
