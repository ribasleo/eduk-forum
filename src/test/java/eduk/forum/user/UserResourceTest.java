package eduk.forum.user;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import eduk.forum.api.service.UserService;
import eduk.forum.config.TestPersistenceConfig;
import eduk.forum.domain.User;
import eduk.forum.dto.NewUserDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration( defaultRollback = true, transactionManager="txManager")
@ContextConfiguration(locations={"file:src/main/resources/spring-config.xml"})
public class UserResourceTest extends TestPersistenceConfig {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	private UserService userService;
	
	Session session = null;
	
	@Before
	public void setUp(){
		super.executeScript();
		session = sessionFactory.openSession();
	}
	
	@After
	public void tearDown(){
		session.close();
	}
	
	@AfterClass
	public static void cleanAll(){
		TestPersistenceConfig.cleanDatabase();
	}
	
	@Test
	@Transactional
	public void testUser(){
		createUser("Eduk", "password", "eduk.forum@test.com");
		
		String hql = "FROM User u";
		Query query = session.createQuery(hql);

		@SuppressWarnings("unchecked")
		List<User> users = query.list();
		assertEquals(1, users.size());
	}
	
	@Test
	@Transactional
	public void testEncryptPassword(){
		NewUserDTO newUserDTO = createNewUserDTO("Eduk", "password", "eduk.forum@test.com");
		
		userService.createUser(newUserDTO);
		
		String hql = "FROM User u";
		Query query = session.createQuery(hql);
		
		User user = (User) query.uniqueResult();
		boolean encryptedPassword = BCrypt.checkpw("password", user.getPassword());
		
		assertEquals(true, encryptedPassword);
	}

	private void createUser(String username, String password, String email) {
		User user = new User();
		user.setName(username);
		user.setPassword(password);
		user.setEmail(email);
		session.save(user);
	}
	
	private NewUserDTO createNewUserDTO(String name, String password, String email){
		NewUserDTO newUser = new NewUserDTO();
		newUser.setUsername(name);
		newUser.setPassword(password);
		newUser.setEmail(email);
		return newUser;
	}
}
