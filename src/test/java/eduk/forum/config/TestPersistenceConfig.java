package eduk.forum.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.Logger;

public class TestPersistenceConfig {

	static final Logger logger = Logger.getLogger(TestPersistenceConfig.class);

	private static final String BEFORE_SQL_SCRIPT_PATH = "src/test/resources/beforeEachTest.sql";

	private static final String AFTER_SQL_SCRIPT_PATH = "src/test/resources/afterClass.sql";
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

	private static final String URL_DATABASE = "jdbc:mysql://localhost:3306/forum_eduk";

	private static final String USER_DATABASE = "eduk";

	private static final String PASSWORD_DATABASE = "edukpass01";


	public void executeScript(){
		try {
			Class.forName(JDBC_DRIVER);
			Connection connection = DriverManager.getConnection(URL_DATABASE, USER_DATABASE, PASSWORD_DATABASE);
			ScriptRunner scriptRunner = new ScriptRunner(connection);

			Reader reader = new BufferedReader(new FileReader(BEFORE_SQL_SCRIPT_PATH));

			scriptRunner.runScript(reader);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public static void cleanDatabase(){
		try {
			Class.forName(JDBC_DRIVER);
			Connection connection = DriverManager.getConnection(URL_DATABASE, USER_DATABASE, PASSWORD_DATABASE);
			ScriptRunner scriptRunner = new ScriptRunner(connection);

			Reader reader = new BufferedReader(new FileReader(AFTER_SQL_SCRIPT_PATH));

			scriptRunner.runScript(reader);
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
