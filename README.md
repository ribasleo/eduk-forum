# Projeto Eduk-Forum

## 1.	Introdução
O projeto foi realizado como parte do processo de seleção para a vaga de Software Engineer na Eduk.  

## 2.	Guia de Configuração Passo-a-Passo
1. Faça o clone do projeto https://ribasleo@bitbucket.org/ribasleo/eduk-forum.git
2. Dentro da pasta do projeto (eduk-forum), navegue até src/main/resources/javamail.properties e altere o valor da propriedade mail.smtp.user e mail.smtp.password respectivamente informando um endereço de email e senha do gmail válidos. 
3. Faça a configuração do banco de dados conforme descrito em [Configuração Banco de Dados](database.md).
4. Após realizar as configurações necessárias, rodar o seguinte comando na pasta raiz do projeto: mvn clean install jetty:run
5. Após o passo anterior, o sistema deverá subir sem erros e portanto será possível utilizá-lo.

## 3.	Guia de Utilização Passo-a-Passo
1. Crie um usuário conforme o serviço descrito em [User](user.md).
2. Para criar um post/thread, utilize o serviço [POST /api/post/create] conforme descrito em [Post](post.md) passando o id do usuário criado no campo "userId".
3. Liste o(s) post(s)/thread(s), utilizando o serviço [GET /api/post/list] conforme descrito em [Post](post.md).
4. Para comentar um post/thread existente, utilize o serviço [POST /api/post/comment] informando o id do root post/thread (rootId), id de um usuário válido (userId) e por fim, o id do post/thread que gostaria de comentar (parentId). 
  
**Note:** Os logs gerados pela aplicação serão gravados em D: na pasta raiz do projeto.

**Dica:** Caso esteja usando o sistema operacional linux, acesso a pasta D: na raiz do projeto e utilize o comando tail -1000f app_log.log para visualizar o log do sistema em tempo real.

## 4.	Banco de Dados (MySQL)

* [Configuração](database.md)

## 5.	Serviços Disponíveis

* [User](user.md)

* [Post](post.md)